import { CommandsService } from './services/commands.service';
import { ClusterService } from './services/cluster.service';
import { initializeMongo } from './utils/mongo';
import { SearchService } from './services/search.service';
import { CleanerService } from './services/cleaner.service';
import { AuthorizationService } from './services/authorization.service';
import { ChangeMenuService } from './services/changeMenu.service';
import { getBuildServices } from './utils/builder';
import { dictionary } from './utils/dictionary';
import { InlineHandlerService } from './services/inlineHandler.service';
import { PreventCommandService } from './services/prevent.service';
import { SendCommandService } from './services/commands/send.command';

(async function () {
  initializeMongo();

  const { inputHandlerService, commandServiceStructure } = await getBuildServices();

  new ClusterService({
    services: [
      new AuthorizationService(),
      new PreventCommandService(dictionary.generate.waitingForResponse),
      new ChangeMenuService(commandServiceStructure),
      new CommandsService({
        structure: commandServiceStructure,
        defaultCommands: [
          {
            command: '/start',
            description: dictionary.defaultCommands.restart,
          },
          {
            command: '/help',
            description: dictionary.defaultCommands.help,
          },
        ],
      }),
      inputHandlerService,
      new InlineHandlerService(),
      new SearchService(commandServiceStructure),
      new CleanerService(),
    ],
  });
})();
