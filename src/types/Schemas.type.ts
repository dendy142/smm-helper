import { Document } from 'mongoose';

export interface Project {
  name: string;
  description: string;
}

export interface User extends Document {
  chatId: number;
  keyboardType: 'default' | 'inline';
  structureMessageId: number | null;
  removeMessagesIds: number[];
  savedMessages: {
    commandName: string;
    messageId: number;
  }[];
  isWaitingForResponse: boolean;
  credits: number;
  settings: {
    language: string;
    useTags: boolean;
    usePlagiarismChecker: boolean;
    currentProjectName: string | null;
    projects: Project[];
    tone: string | null;
    format: string | null;
  };
}
