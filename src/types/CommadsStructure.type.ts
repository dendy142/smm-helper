import { CommandServiceTool } from './tools/CommandService.tool';
import { ConditionCommandsService } from '../services/commands/condition.commands';
import { UnionMessageType } from './Message.type';
import TelegramBot from 'node-telegram-bot-api';
import { ClusterContext } from './ClusterContext.type';

export type CommandFunctionName = () => Promise<string> | string;

export type CommandsStructure = {
  commandName: string | CommandFunctionName;
  row: number;
  hidden?: boolean | ((msg: TelegramBot.Message) => Promise<boolean> | boolean);
  saveMessage?: boolean;
  pin?: boolean;
  search?: string | string[];
  structureMessage?: UnionMessageType;
  structure?: CommandsStructureFunction;
  commandService?: CommandServiceTool | CommandServiceTool[];
  commandCondition?: ConditionCommandsService;
}[];
export type CommandsStructureFunction =
  | CommandsStructure
  | ((context: ClusterContext) => Promise<CommandsStructure> | CommandsStructure);
