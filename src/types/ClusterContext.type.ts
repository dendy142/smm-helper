import TelegramBot from 'node-telegram-bot-api';
import { CommandsStructure } from './CommadsStructure.type';

export enum EventNames {
  'loopEnd',
}

export interface ClusterContext {
  chatId: number;
  usersCount: number;
  commandStructure: CommandsStructure[number] | null;

  dispatchCallbackQuery: (context: ClusterContext, query: TelegramBot.CallbackQuery) => void;
  addEventListener: (eventName: EventNames, callback: () => any) => () => void;
  dontRemoveMessage?: boolean;
}
