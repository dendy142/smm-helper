import TelegramBot from 'node-telegram-bot-api';
import { ClusterContext } from '../ClusterContext.type';

export abstract class ServiceTool {
  protected readonly serviceName: string;
  protected telegramBot?: TelegramBot;
  protected context?: ClusterContext;

  constructor() {
    this.serviceName = this.constructor.name;
  }

  public setOptions(options: { telegramBot?: TelegramBot; context?: ClusterContext }): this {
    this.telegramBot = options.telegramBot ?? this.telegramBot;
    this.context = options.context ?? this.context;

    return this;
  }

  protected getOptions(): {
    telegramBot: TelegramBot;
    context: ClusterContext;
  } {
    const { telegramBot, context } = this.validateProperties({
      telegramBot: this.telegramBot,
      context: this.context,
    });

    return {
      telegramBot,
      context,
    };
  }
  protected validateProperties<T extends { [key: string]: any }>(
    properties: T,
  ): {
    [key in keyof T]: Exclude<T[key], undefined | null>;
  } {
    const entities = Object.entries(properties);
    for (const [key, value] of entities) {
      if (value == null) {
        this.throwPropertyNotInitialized(key);
      }
    }
    return properties as Required<T>;
  }
  protected throwPropertyNotInitialized(propertyName: string): never {
    throw new Error(`${this.serviceName} ${propertyName} not initialized`);
  }
}
