import TelegramBot from 'node-telegram-bot-api';
import { CommandsStructureFunction, CommandsStructure } from '../CommadsStructure.type';
import { ServiceTool } from './Service.tool';

export abstract class CommandServiceTool extends ServiceTool {
  public isMessageSender: boolean = false;
  protected commandsStructure?: CommandsStructureFunction;

  public setOptions(
    options: Parameters<ServiceTool['setOptions']>[number] & {
      commandsStructure?: CommandsStructureFunction;
    },
  ): this {
    super.setOptions(options);

    this.commandsStructure = options.commandsStructure ?? this.commandsStructure;

    return this;
  }
  public abstract action(msg: TelegramBot.Message, structure: CommandsStructure[number]): Promise<void>;

  protected getOptions(): ReturnType<ServiceTool['getOptions']> & {
    commandsStructure?: CommandsStructureFunction;
  } {
    const options = super.getOptions();

    return {
      ...options,
      commandsStructure: this.commandsStructure,
    };
  }
}
