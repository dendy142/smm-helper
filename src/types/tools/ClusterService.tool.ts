import TelegramBot from 'node-telegram-bot-api';
import { ServiceTool } from './Service.tool';

export abstract class ClusterServiceTool extends ServiceTool {
  public onMessage?(msg: TelegramBot.Message): Promise<boolean>;
  public onCallbackQuery?(query: TelegramBot.CallbackQuery): Promise<boolean>;
  public onStopPropagation?(options: {
    message?: TelegramBot.Message;
    query?: TelegramBot.CallbackQuery;
  }): Promise<void>;
}
