import TelegramBot from 'node-telegram-bot-api';
import { CommandsStructureFunction } from '../CommadsStructure.type';
import { ServiceTool } from './Service.tool';

export abstract class InputHandlerServiceTool extends ServiceTool {
  protected commandsStructure?: CommandsStructureFunction;

  public setOptions(
    options: Parameters<ServiceTool['setOptions']>[0] & {
      commandsStructure?: CommandsStructureFunction;
    },
  ): this {
    super.setOptions(options);

    this.commandsStructure = options.commandsStructure ?? this.commandsStructure;

    return this;
  }
  public abstract activate(msg: TelegramBot.Message): Promise<boolean>;

  protected getOptions(): ReturnType<ServiceTool['getOptions']> & {
    commandsStructure: CommandsStructureFunction;
  } {
    const options = super.getOptions();

    const { commandsStructure } = this.validateProperties({
      commandsStructure: this.commandsStructure,
    });

    return {
      ...options,
      commandsStructure,
    };
  }
}
