import { initializeMongo } from '../utils/mongo';

const migrations: {
  up?: () => Promise<void>;
  down?: () => Promise<void>;
}[] = [];

// get first argument from cli prompt
const [command] = process.argv.slice(2);
console.log(`Running migration: ${command}`);

initializeMongo('localhost');

(async function init(commandName) {
  switch (commandName) {
    case 'up':
      for (const migration of migrations) {
        if ('up' in migration) {
          await migration.up?.();
        }
      }

      console.log('Migrations up');
      break;

    case 'down':
      for (const migration of migrations.reverse()) {
        if ('down' in migration) {
          await migration.down?.();
        }
      }

      console.log('Migrations down');
      break;

    case 'reset':
      await init('down');
      await init('up');

      console.log('Migrations reset');
      break;
  }

  if (command === commandName) {
    process.exit();
  }
})(command);
