import { FilterQuery, model, Schema } from 'mongoose';
import { User } from '../types/Schemas.type';

export const userSchema = {
  chatId: {
    required: true,
    type: Number,
    unique: true,
  },
  keyboardType: {
    required: true,
    default: 'default',
    enum: ['default', 'inline'],
    type: String,
  },
  structureMessageId: {
    type: Number,
    default: null,
  },
  removeMessagesIds: {
    type: [Number],
    default: [],
  },
  savedMessages: {
    type: [
      {
        commandName: String,
        messageId: Number,
      },
    ],
  },
  isWaitingForResponse: {
    required: true,
    default: false,
    type: Boolean,
  },
  credits: {
    required: false,
    default: 100,
    type: Number,
  },
  settings: {
    language: {
      required: true,
      default: 'russian',
      type: String,
    },
    useTags: Boolean,
    usePlagiarismChecker: Boolean,
    projects: [
      {
        required: false,
        default: [],
        type: {
          name: {
            required: true,
            type: String,
          },
          description: {
            required: false,
            type: String,
            default: null,
          },
        },
      },
    ],
    currentProjectName: {
      required: false,
      type: String,
    },
    tone: {
      required: false,
      type: String,
      default: null,
    },
    format: {
      required: false,
      type: String,
      default: null,
    },
  },
};

const schema = new Schema(userSchema);

export const userModel = model<User>('User', schema);

export const updateUser = async (chatId: number, data: FilterQuery<User>) => {
  return userModel.updateOne({ chatId }, data);
};

export const updateUsers = async (options: FilterQuery<User>, data: FilterQuery<User>) => {
  return userModel.updateMany(options, data);
};

export const getUser = async (chatId: number): Promise<User> => {
  const user = await userModel.findOne({ chatId });
  if (user == null) {
    throw new Error(`User with chatId ${chatId} not found`);
  }
  return user.toObject();
};

export const createUser = async (chatId: number) => {
  return userModel.create({ chatId });
};

export const getUsersCount = () => {
  return userModel.count({});
};
