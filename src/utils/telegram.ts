import TelegramBot from 'node-telegram-bot-api';
import { config } from '../config';

export const createTelegramBot = (): TelegramBot => {
  return new TelegramBot(config.telegram.token, {
    polling: true,
  });
};

export const escapeCharactersForTelegram = (text: string) => {
  return text.replace(/([_\-*[\]()~`>#+=|{}.!])/g, '\\$1');
};
