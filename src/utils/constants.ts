export const MAX_PROJECTS_LENGTH = 3;
export const MAX_SYMBOLS_IN_PROJECT_NAME = 30;
export const MAX_WORDS_IN_PROJECT_NAME = 5;
export const MAX_SYMBOLS_IN_PROJECT_DESCRIPTION = 3000;
export const PRICE_FOR_CREDITS = {
  regular: 1,
};

export const DELETE_KEY_WORD = 'я уверен';
export const REPEAT_COMPLETION_COMMAND = 'repeat-completion';
export const CHANGE_LANGUAGE_COMMAND = 'change-language';
export const USE_TAGS_COMMAND = 'use-tags';
export const USE_PLAGIARISM_CHECKED_COMMAND = 'use-plagiarism-checker';

export const TONES = {
  humorous: 'set-tone-humorous',
  informative: 'set-tone-informative',
  formal: 'set-tone-formal',
  joyful: 'set-tone-joyful',
  sad: 'set-tone-sad',
  angry: 'set-tone-angry',
  convincing: 'set-tone-convincing',
};

export const FORMATS = {
  socialMedia: 'set-format-social-media',
  description: 'set-format-description',
  email: 'set-format-email',
  faq: 'set-format-faq',
  idea: 'set-format-idea',
  improve: 'set-format-improve',
  supplement: 'set-format-supplement',
  cut: 'set-format-cut',
  paraphrase: 'set-format-paraphrase',
};
