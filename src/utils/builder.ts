import { CommandsStructureFunction, CommandsStructure } from '../types/CommadsStructure.type';
import { dictionary } from './dictionary';
import { SendCommandService } from '../services/commands/send.command';
import { InputHandlerService } from '../services/inputHandler.service';
import { BackCommandService } from '../services/commands/back.command';
import { InputHandlerCommandService } from '../services/commands/inputHandler.command';
import { ProjectInputHandlerService } from '../services/inputHandlers/project.inputHandler';
import { SetProjectCommandService } from '../services/commands/setProject.command';
import { getUser } from '../models/user.model';
import { ProjectDescriptionInputHandlerService } from '../services/inputHandlers/projectDescription.inputHandler';
import { ProjectDeleteInputHandlerService } from '../services/inputHandlers/projectDelete.inputHandler';
import { AggregatorCommandService } from '../services/commands/aggregator.command';
import { GenerateInputHandlerService } from '../services/inputHandlers/generate.inputHandler';
import { PostSettingsCommandService } from '../services/commands/postSettings.command';
import { MAX_PROJECTS_LENGTH } from './constants';
import { FormatSettingsCommand } from '../services/commands/formatSettings.command';
import { ToneSettingsCommand } from '../services/commands/toneSettings.command';

let buildServicesCreated = false;
export const getBuildServices = async (): Promise<{
  inputHandlerService: InputHandlerService;
  commandServiceStructure: CommandsStructureFunction;
}> => {
  if (buildServicesCreated) {
    throw new Error('Services already created');
  }
  buildServicesCreated = true;

  const inputHandlerService = new InputHandlerService();

  const commandServiceStructure = [
    {
      row: 0,
      commandName: dictionary.startBot,
      commandService: new SendCommandService({
        saveMessage: true,
        message: {
          text: dictionary.welcome.text,
        },
      }),
      structureMessage: dictionary.backBtnMessage,
      structure: [
        {
          row: 0,
          commandName: dictionary.generate.commandName,
          commandService: new AggregatorCommandService([
            new ToneSettingsCommand(),
            new FormatSettingsCommand(),
            new InputHandlerCommandService(
              inputHandlerService,
              new GenerateInputHandlerService({
                backCommandService: new BackCommandService({
                  to: dictionary.startBot,
                  message: `${dictionary.generate.backMessage}\n\n${dictionary.backBtnMessage}`,
                }),
              }),
            ),
          ]),
          structureMessage: dictionary.generate.message,
          structure: [
            {
              row: 5,
              commandName: dictionary.backBtn,
              commandService: new BackCommandService({
                to: dictionary.startBot,
                message: dictionary.backBtnMessage,
              }),
            },
          ],
        },
        {
          row: 0,
          commandName: dictionary.settings.commandName,
          search: dictionary.settings.commandName,
          structure: [
            {
              row: 0,
              commandName: dictionary.settings.projects.commandName,
              search: dictionary.settings.projects.commandName,
              structure: async (context) => {
                const { chatId } = context;
                const user = await getUser(chatId);

                const projects = user.settings.projects;
                const currentProjectName = user.settings.currentProjectName;

                const result: CommandsStructure = projects.map((project, i) => {
                  const isDefault = currentProjectName === project.name;

                  return {
                    row: i % 2,
                    commandName: dictionary.settings.projects.project.commandName(project.name, isDefault),
                    search: dictionary.settings.projects.project.commandName(project.name, isDefault),
                    structureMessage: `${dictionary.placeholders.choose(
                      dictionary.settings.projects.project.structureName,
                    )}\n\n${dictionary.settings.projects.project.projectInfo(
                      project.name,
                      isDefault,
                      project.description,
                    )}`,
                    structure: [
                      {
                        row: 0,
                        commandName: dictionary.settings.projects.project.edit.commandName,
                        commandService: new InputHandlerCommandService(
                          inputHandlerService,
                          new ProjectDescriptionInputHandlerService({
                            projectName: project.name,
                            backCommandService: new BackCommandService({
                              to: dictionary.settings.projects.commandName,
                              message: `${dictionary.placeholders.choose(
                                dictionary.settings.projects.commandName,
                              )}\n\n${dictionary.settings.projects.project.edit.success(project.name)}`,
                            }),
                          }),
                        ),
                        structureMessage: dictionary.settings.projects.project.edit.message,
                        structure: [
                          {
                            row: 5,
                            commandName: dictionary.settings.projects.backBtn,
                            commandService: new BackCommandService({
                              to: dictionary.settings.projects.commandName,
                            }),
                          },
                        ],
                      },
                      {
                        row: 1,
                        commandName: isDefault
                          ? dictionary.settings.projects.project.unset.commandName
                          : dictionary.settings.projects.project.set.commandName,
                        commandService: new SetProjectCommandService({
                          projectName: project.name,
                          backCommandService: new BackCommandService({
                            to: dictionary.settings.projects.commandName,
                            message: isDefault
                              ? `${dictionary.placeholders.choose(dictionary.settings.projects.commandName)}\n\n${
                                  dictionary.settings.projects.project.unset.success
                                }`
                              : `${dictionary.placeholders.choose(dictionary.settings.projects.commandName)}\n\n${
                                  dictionary.settings.projects.project.set.success
                                }`,
                          }),
                        }),
                      },
                      {
                        row: 1,
                        commandName: dictionary.settings.projects.project.delete.commandName,
                        commandService: new InputHandlerCommandService(
                          inputHandlerService,
                          new ProjectDeleteInputHandlerService({
                            projectName: project.name,
                            backCommandService: new BackCommandService({
                              to: dictionary.settings.projects.commandName,
                              message: `${dictionary.placeholders.choose(
                                dictionary.settings.projects.commandName,
                              )}\n\n${dictionary.settings.projects.project.delete.success(project.name)}`,
                            }),
                          }),
                        ),
                        structureMessage: dictionary.settings.projects.project.delete.message,
                        structure: [
                          {
                            row: 5,
                            commandName: dictionary.settings.projects.backBtn,
                            commandService: new BackCommandService({
                              to: dictionary.settings.projects.commandName,
                            }),
                          },
                        ],
                      },
                      {
                        row: 5,
                        commandName: dictionary.settings.projects.backBtn,
                        commandService: new BackCommandService({
                          to: dictionary.settings.projects.commandName,
                        }),
                      },
                    ],
                  };
                });

                if (projects.length < MAX_PROJECTS_LENGTH) {
                  result.push({
                    row: 1,
                    commandName: dictionary.settings.projects.add.commandName,
                    structureMessage: dictionary.settings.projects.add.message,
                    structure: [
                      {
                        row: 5,
                        commandName: dictionary.settings.projects.backBtn,
                        commandService: new BackCommandService({
                          to: dictionary.settings.projects.commandName,
                        }),
                      },
                    ],
                    commandService: new InputHandlerCommandService(
                      inputHandlerService,
                      new ProjectInputHandlerService({
                        backCommandService: new BackCommandService({
                          to: dictionary.settings.projects.commandName,
                          message: dictionary.settings.projects.addedSuccessful,
                        }),
                      }),
                    ),
                  });
                }

                result.push({
                  row: 5,
                  commandName: dictionary.settings.backBtn,
                  commandService: new BackCommandService({
                    to: dictionary.settings.commandName,
                  }),
                });

                return result;
              },
            },
            {
              row: 0,
              commandName: dictionary.settings.post.commandName,
              commandService: new PostSettingsCommandService(),
            },
            {
              row: 5,
              commandName: dictionary.backBtn,
              commandService: new BackCommandService({
                to: dictionary.startBot,
                message: dictionary.backBtnMessage,
              }),
            },
          ],
        },
        {
          row: 1,
          commandName: dictionary.credits.commandName,
          search: dictionary.credits.commandName,
          commandService: new SendCommandService({
            message: async (context) => {
              const { chatId } = context;
              const user = await getUser(chatId);

              return {
                text: dictionary.credits.balance(user.credits),
                reply_markup: {
                  inline_keyboard: [
                    [
                      {
                        text: dictionary.credits.purchase1.commandName,
                        url: 'https://ass.com',
                      },
                      {
                        text: dictionary.credits.purchase2.commandName,
                        url: 'https://ass.com',
                      },
                    ],
                    [
                      {
                        text: dictionary.credits.purchase3.commandName,
                        url: 'https://ass.com',
                      },
                      {
                        text: dictionary.credits.purchase4.commandName,
                        url: 'https://ass.com',
                      },
                    ],
                  ],
                },
              };
            },
          }),
        },
        {
          row: 1,
          commandName: dictionary.help.commandName,
          commandService: new SendCommandService({
            message: dictionary.help.message,
          }),
        },
      ],
    },
  ];

  return {
    inputHandlerService,
    commandServiceStructure,
  };
};
