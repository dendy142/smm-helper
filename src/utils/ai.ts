import { config } from '../config';
import { ChatGPTAPI as ChatGPTAPIType } from 'chatgpt';
import { dictionary } from './dictionary';
import { escapeCharactersForTelegram } from './telegram';
import { PRICE_FOR_CREDITS, REPEAT_COMPLETION_COMMAND } from './constants';
import { User } from '../types/Schemas.type';
import TelegramBot from 'node-telegram-bot-api';
import { updateUser } from '../models/user.model';

const _importDynamic = new Function('modulePath', 'return import(modulePath)');
let ChatGPTAPI: new (any) => ChatGPTAPIType;

let initModulesWaiter: Promise<void> | undefined = undefined;
const initModules = async () => {
  if (initModulesWaiter == null) {
    initModulesWaiter = new Promise(async (resolve) => {
      if (globalThis.fetch == null) {
        const { default: fetch } = await _importDynamic('node-fetch');
        globalThis.fetch = fetch;
      }

      if (ChatGPTAPI == null) {
        const chatgpt = await _importDynamic('chatgpt');
        ChatGPTAPI = chatgpt.ChatGPTAPI;
      }

      resolve();
    });
  }

  return initModulesWaiter;
};

let aiChat: ChatGPTAPIType | undefined = undefined;
export const getAIChat = async () => {
  await initModules();

  return (
    aiChat ??
    (aiChat = new ChatGPTAPI({
      apiKey: config.openai.token,
    }))
  );
};

export const sendCompletionWithSideEffects = async (
  telegramBot: TelegramBot,
  user: User,
  replyId: number,
  text: string,
  callback?: (data: string) => void,
): Promise<boolean> => {
  const { chatId } = user;

  if (user.credits < PRICE_FOR_CREDITS.regular) {
    await telegramBot.sendMessage(chatId, dictionary.generate.noCredits);
    return false;
  }

  if (user.structureMessageId != null) {
    try {
      await telegramBot.deleteMessage(chatId, user.structureMessageId.toString());
    } catch (err) {}
  }
  await updateUser(chatId, {
    isWaitingForResponse: true,
    structureMessageId: null,
  });

  const responseMessage = await telegramBot.sendMessage(chatId, dictionary.generate.generationWaiting(user.settings), {
    reply_to_message_id: replyId,
  });

  const { useTags, usePlagiarismChecker, currentProjectName, projects, tone, format } = user.settings;
  let prompt = `I want you to act as an SMM manager. You will create a promotion text based on my prompt, ${
    useTags ? 'be sure to use hashtags' : "don't use hashtags even if I ask you so"
  }. `;

  if (currentProjectName != null) {
    const project = projects.find((project) => project.name === currentProjectName);
    if (project == null) {
      throw new Error(`Project with name ${currentProjectName} not found`);
    }

    prompt += `I want you to depend on project context, project name is "${project.name}". `;
    if (project.description != null) {
      prompt += `Project description is "${project.description}". `;
    }
  }

  if (tone != null) {
    prompt += `I want you to use ${tone} tone. `;
  }
  if (format != null) {
    prompt += `I want you to use ${format} format. `;
  }

  prompt += `You will answer on ${user.settings.language} language only, my text is "${text}"`;

  console.log('PROMPT', prompt);

  try {
    const aiChat = await getAIChat();
    void aiChat.sendMessage(prompt).then(async (data) => {
      const result = data.text;

      await updateUser(chatId, {
        isWaitingForResponse: false,
        $inc: {
          credits: -PRICE_FOR_CREDITS.regular,
        },
      });

      await telegramBot.editMessageText(`\`${escapeCharactersForTelegram(result)}\``, {
        chat_id: chatId,
        message_id: responseMessage.message_id,
        parse_mode: 'MarkdownV2',
        reply_markup: {
          inline_keyboard: [
            [
              {
                text: dictionary.generate.repeat,
                callback_data: REPEAT_COMPLETION_COMMAND,
              },
            ],
          ],
        },
      });

      callback?.(result);
    });
  } catch (err) {
    await telegramBot.editMessageText(dictionary.internalError, {
      chat_id: chatId,
      message_id: responseMessage.message_id,
    });
    throw err;
  }

  return true;
};
