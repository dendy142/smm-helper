import {
  DELETE_KEY_WORD,
  MAX_SYMBOLS_IN_PROJECT_DESCRIPTION,
  MAX_SYMBOLS_IN_PROJECT_NAME,
  MAX_WORDS_IN_PROJECT_NAME,
} from './constants';
import { User } from '../types/Schemas.type';
import { Languages } from './settings';

export const dictionary = {
  startBot: '/start',
  backBtn: '💻 Назад',
  backBtnMessage: 'Вы находитесь в "Главном меню 💻".\nВыберите подходящий пункт меню.',
  welcome: {
    text: 'Я помогу написать любой текст для Вашего проекта.',
  },
  internalError: 'Произошла ошибка, попробуй еще раз.',
  generate: {
    commandName: 'Генерация 🔄',
    backMessage: 'Текст успешно сгенерирован, вы найдете его выше.',
    message:
      'Выберите настройки для генерации текста и напишите описание для генерируемого текста, либо вернитесь обратно.',
    waitingForResponse: 'Дождитесь ответа от предыдущего запроса.',
    noCredits: 'У вас недостаточно кредитов для генерации текста.',
    generationWaiting: (settings: User['settings']) =>
      `Пожалуйста, подождите (~10 сек)...\n\nНастройки:${
        settings.currentProjectName == null ? '' : `\n- проект: ${settings.currentProjectName}`
      }\n- язык ответа: ${Languages[settings.language]}\n- ${settings.useTags ? 'с тэгами' : 'без тэгов'}${
        settings.tone == null ? '' : `\n- тон: ${dictionary.generate.tones[settings.tone](false)}`
      }${settings.format == null ? '' : `\n- формат: ${dictionary.generate.formats[settings.format](false)}`}`,
    repeat: 'Повторить 🔁',
    next: {
      commandName: 'Продолжить ⏩',
    },
    tones: {
      message: 'Выберите тон текста.',
      humorous: (enabled: boolean) => `${enabled ? '✅' : ''} Юмористический`,
      informative: (enabled: boolean) => `${enabled ? '✅' : ''} Информативный`,
      formal: (enabled: boolean) => `${enabled ? '✅' : ''} Формальный`,
      joyful: (enabled: boolean) => `${enabled ? '✅' : ''} Радостный`,
      sad: (enabled: boolean) => `${enabled ? '✅' : ''} Грустный`,
      angry: (enabled: boolean) => `${enabled ? '✅' : ''} Злой`,
      convincing: (enabled: boolean) => `${enabled ? '✅' : ''} Убедительный`,
    },
    formats: {
      message: 'Выберите формат текста.',
      socialMedia: (enabled: boolean) => `${enabled ? '✅' : ''} Социальные сети`,
      description: (enabled: boolean) => `${enabled ? '✅' : ''} Описание`,
      email: (enabled: boolean) => `${enabled ? '✅' : ''} Письмо`,
      faq: (enabled: boolean) => `${enabled ? '✅' : ''} FAQ`,
      idea: (enabled: boolean) => `${enabled ? '✅' : ''} Идея`,
      improve: (enabled: boolean) => `${enabled ? '✅' : ''} Улучшить`,
      supplement: (enabled: boolean) => `${enabled ? '✅' : ''} Дополненить`,
      cut: (enabled: boolean) => `${enabled ? '✅' : ''} Сократить`,
      paraphrase: (enabled: boolean) => `${enabled ? '✅' : ''} Перефразировать`,
    },
  },
  credits: {
    commandName: 'Кредиты 💰',
    balance: (amount: number) => `Ваш баланс: ${amount} кредитов`,
    purchase1: {
      commandName: 'Купить 10 кредитов',
    },
    purchase2: {
      commandName: 'Купить 50 кредитов',
    },
    purchase3: {
      commandName: 'Купить 100 кредитов',
    },
    purchase4: {
      commandName: 'Купить 1000 кредитов',
    },
  },
  settings: {
    commandName: 'Настройки ⚙️',
    backBtn: '⚙️ Назад',
    projects: {
      commandName: 'Проекты 📽️',
      backBtn: '📽 Назад',
      project: {
        commandName: (name: string, isDefault: boolean) => `Проект "${name}"${isDefault ? ' ✅' : ''}`,
        structureName: 'Проект 📷️',
        projectInfo: (name: string, isDefault: boolean, description?: string) =>
          `Проект "${name}"${isDefault ? ' (по умолчанию)' : ''}.${
            description != null ? `\nОписание: "${description}".` : ''
          }`,
        edit: {
          commandName: 'Редактировать описание  📷️',
          message: `Введите описание проекта длиной не более ${MAX_SYMBOLS_IN_PROJECT_DESCRIPTION} символов.`,
          invalid: `Длина описания не должна превышать ${MAX_SYMBOLS_IN_PROJECT_DESCRIPTION} символов.`,
          success: (name: string) => `Описание проекта "${name}" успешно изменено.`,
        },
        delete: {
          commandName: 'Удалить  📷️',
          message: `Введите "${DELETE_KEY_WORD}", если уверены, что хотите удалить проект.`,
          invalid: 'Неправильное контрольное слово.',
          success: (name: string) => `Проект "${name}" успешно удален.`,
        },
        set: {
          commandName: 'Использовать  📷️',
          success: `Проект успешно установлен по умолчанию.`,
        },
        unset: {
          commandName: 'Не использовать  📷️',
          success: 'Проект успешно убран из списка используемых.',
        },
      },
      add: {
        commandName: 'Добавить проект 📽️',
        message: `Напишите название нового проекта, имя не должно превышать ${MAX_SYMBOLS_IN_PROJECT_NAME} символов и ${MAX_WORDS_IN_PROJECT_NAME} слов.`,
      },
      invalid: `Имя проекта не должно превышать длину ${MAX_SYMBOLS_IN_PROJECT_NAME} символов и ${MAX_WORDS_IN_PROJECT_NAME} слов.`,
      addedSuccessful: 'Проект успешно добавлен, теперь вы можете установить описание для него.',
    },
    post: {
      commandName: 'Настройка постов 🔧',
      message: 'Выберите настройки для генерации постов.',
      language: (lang: string) => `Язык ответа: ${lang}`,
      useTags: (enabled: boolean) => `Использовать хэштеги ${enabled ? '✅' : '❌'}`,
      usePlagiarismChecker: (enabled: boolean) => `Проверять на плагиат ${enabled ? '✅' : '❌'}`,
    },
  },
  help: {
    commandName: 'Помощь ❓',
    message: 'Здесь вы ее не найдете.',
  },
  placeholders: {
    choose: (catalogName: string) =>
      `Вы находитесь в разделе "${catalogName}".\nВыберите подходящий раздел или интересующую вас услугу.`,
    searchVariants: (command: string) => `По запросу "${command}" были подобраны наиболее похожие варианты:`,
    nothingFound: (command: string) =>
      `По запросу "${command}" ничего не найдено.\nИспользуйте навигационное меню или воспользуйтесь помощью:`,
    sendCommandService: 'Выберите подходящий пункт меню',
    more: (emoji: string) => ({
      commandName: `${emoji} Еще`,
      message: 'Полный список',
    }),
  },
  defaultCommands: {
    help: 'Помощь по боту',
    restart: 'Перезапустить бота',
  },
};
