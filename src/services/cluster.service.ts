import TelegramBot from 'node-telegram-bot-api';
import { createTelegramBot } from '../utils/telegram';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { ClusterContext, EventNames } from '../types/ClusterContext.type';
import { getUsersCount } from '../models/user.model';

export class ClusterService {
  private readonly telegramBot: TelegramBot;
  private readonly events: { [key in EventNames]: { callback: () => any; chatId: number }[] } = {
    [EventNames.loopEnd]: [],
  };
  private services: ClusterServiceTool[] = [];

  constructor(options: { services: ClusterServiceTool[] }) {
    this.dispatchCallbackQuery = this.dispatchCallbackQuery.bind(this);

    const telegramBot = (this.telegramBot = createTelegramBot());

    telegramBot.on(
      'callback_query',
      this.triggerServicesBind<TelegramBot.CallbackQuery>({
        getIdCallback: (query) => query.message?.chat.id ?? 0,
        actionCallback: async (service, query) => {
          return service.onCallbackQuery?.(query) ?? false;
        },
        stopPropagationCallback: async (service, query) => {
          await service.onStopPropagation?.({
            query,
          });
        },
      }),
    );
    telegramBot.on(
      'message',
      this.triggerServicesBind<TelegramBot.Message>({
        getIdCallback: (query) => query.chat.id,
        actionCallback: async (service, message) => {
          return service.onMessage?.(message) ?? false;
        },
        stopPropagationCallback: async (service, message) => {
          await service.onStopPropagation?.({
            message,
          });
        },
      }),
    );

    for (const service of options.services) {
      this.addService(service);
    }
  }

  private async dispatchCallbackQuery(context: ClusterContext, query: TelegramBot.CallbackQuery) {
    const chatId = query.message?.chat.id;
    if (chatId == null) {
      return;
    }

    let isStopPropagate = false;
    for (const service of this.services) {
      service.setOptions({ context, telegramBot: this.telegramBot });

      if (isStopPropagate) {
        await service.onStopPropagation?.({
          query,
        });
      } else if (await service.onCallbackQuery?.(query)) {
        isStopPropagate = true;
      }
    }

    this.triggerEvent(chatId, EventNames.loopEnd);
  }

  private triggerServicesBind<T extends TelegramBot.Message | TelegramBot.CallbackQuery>(options: {
    getIdCallback: (data: T) => number;
    actionCallback: (service: ClusterServiceTool, data: T) => Promise<boolean>;
    stopPropagationCallback: (service: ClusterServiceTool, data: T) => Promise<void>;
  }): (data: T) => Promise<void> {
    return async (data: T) => {
      const chatId = options.getIdCallback(data);

      const context: ClusterContext = {
        chatId,
        usersCount: await getUsersCount(),
        commandStructure: null,

        dispatchCallbackQuery: this.dispatchCallbackQuery,
        addEventListener: (eventName, callback) => this.addEventListener(chatId, eventName, callback),

        dontRemoveMessage: false,
      };

      let isStopPropagate = false;
      for (const service of this.services) {
        service.setOptions({ context, telegramBot: this.telegramBot });

        if (isStopPropagate) {
          await options.stopPropagationCallback(service, data);
        } else if (await options.actionCallback(service, data)) {
          isStopPropagate = true;
        }
      }

      this.triggerEvent(chatId, EventNames.loopEnd);
    };
  }

  private triggerEvent(targetChatId: number, eventName: EventNames): void {
    const callbacks = [...this.events[eventName]];
    for (const { callback, chatId } of callbacks) {
      if (targetChatId === chatId) {
        callback();
      }
    }
  }

  private addEventListener(chatId: number, eventName: EventNames, callback: () => any): () => void {
    const eventInfo = {
      callback,
      chatId,
    };

    this.events[eventName].push(eventInfo);
    return () => {
      const index = this.events[eventName].indexOf(eventInfo);
      this.events[eventName].splice(index, 1);
    };
  }

  private addService(service: ClusterServiceTool): void {
    this.services.push(service);
  }
}
