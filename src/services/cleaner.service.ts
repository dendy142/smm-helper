import TelegramBot from 'node-telegram-bot-api';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { getUser, updateUser } from '../models/user.model';

export class CleanerService extends ClusterServiceTool {
  async onStopPropagation({ message, query }) {
    await this.onMessage(message ?? query.message);
  }

  async onMessage({ message_id, chat: { id: chatId }, text }: TelegramBot.Message) {
    const { telegramBot, context } = this.getOptions();

    const user = await getUser(chatId);
    if (user == null) {
      return false;
    }

    let removeMessagesIds = (context.dontRemoveMessage === true ? [] : [message_id]).concat(
      user?.removeMessagesIds ?? [],
    );
    if (user.structureMessageId != null) {
      removeMessagesIds = removeMessagesIds.filter((id) => id !== user.structureMessageId);
    }

    for (const messageId of removeMessagesIds) {
      try {
        await telegramBot.deleteMessage(chatId, messageId.toString());
      } catch (err) {}
    }

    const lastMessagesIdsSet = new Set(removeMessagesIds);
    const lastMessagesIds = ((await getUser(chatId)).removeMessagesIds ?? []).filter(
      (id) => !lastMessagesIdsSet.has(id),
    );

    await updateUser(chatId, {
      removeMessagesIds: lastMessagesIds,
    });

    return false;
  }
}
