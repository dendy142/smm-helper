import TelegramBot from 'node-telegram-bot-api';
import { getUser, updateUser } from '../models/user.model';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { CommandsStructureFunction } from '../types/CommadsStructure.type';
import { StructureCommandService } from './commands/structure.command';

export class ChangeMenuService extends ClusterServiceTool {
  private readonly structureCommandService: StructureCommandService;

  constructor(private readonly structure: CommandsStructureFunction) {
    super();

    this.structureCommandService = new StructureCommandService({
      structure,
    });
  }

  setOptions(options): this {
    super.setOptions(options);

    void this.structureCommandService.setOptions(options);

    return this;
  }

  async onCallbackQuery(query: TelegramBot.CallbackQuery): Promise<boolean> {
    if (query.message == null) {
      return false;
    }

    const { data, message } = query;
    const {
      chat: { id: chatId },
    } = message;
    if (data !== 'changeMenu') {
      return false;
    }

    const { context } = this.getOptions();

    const user = await getUser(chatId);

    const keyboardType = user.keyboardType;
    await updateUser(chatId, { keyboardType: keyboardType === 'inline' ? 'default' : 'inline' });

    const commandStructure = this.structure[0];

    let command;
    if (typeof commandStructure.commandName === 'function') {
      command = commandStructure.commandName();
    } else {
      command = commandStructure.commandName;
    }

    void context.dispatchCallbackQuery(context, {
      ...query,
      data: command,
    });

    return true;
  }
}
