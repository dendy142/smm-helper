import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { SendCommandService } from '../commands/send.command';
import { BackCommandService } from '../commands/back.command';
import { dictionary } from '../../utils/dictionary';
import { getUser } from '../../models/user.model';
import { sendCompletionWithSideEffects } from '../../utils/ai';

export class GenerateInputHandlerService extends InputHandlerServiceTool {
  private readonly sendCommandService: SendCommandService;
  private readonly backCommandService?: BackCommandService;

  constructor(options: { backCommandService?: BackCommandService }) {
    super();

    this.sendCommandService = new SendCommandService({
      message: dictionary.settings.projects.project.delete.invalid,
    });
    this.backCommandService = options.backCommandService;
  }

  setOptions(opts) {
    super.setOptions(opts);

    const options = this.getOptions();

    this.sendCommandService?.setOptions(options);
    this.backCommandService?.setOptions(options);

    return this;
  }

  public async activate(msg: TelegramBot.Message): Promise<boolean> {
    const {
      text,
      message_id,
      chat: { id: chatId },
    } = msg;
    if (text == null) {
      return false;
    }

    const { context } = this.getOptions();
    const { telegramBot } = this.validateProperties({
      telegramBot: this.telegramBot,
    });

    const user = await getUser(chatId);

    await sendCompletionWithSideEffects(telegramBot, user, message_id, text, () => {
      this.backCommandService?.action(msg);
    });

    context.dontRemoveMessage = true;

    return true;
  }
}
