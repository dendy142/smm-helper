import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { SendCommandService } from '../commands/send.command';
import { BackCommandService } from '../commands/back.command';
import { dictionary } from '../../utils/dictionary';
import { getUser, updateUser } from '../../models/user.model';
import { MAX_SYMBOLS_IN_PROJECT_DESCRIPTION } from '../../utils/constants';

export class ProjectDescriptionInputHandlerService extends InputHandlerServiceTool {
  private readonly sendCommandService: SendCommandService;
  private readonly backCommandService?: BackCommandService;
  private readonly projectName: string;

  constructor(options: { backCommandService?: BackCommandService; projectName: string }) {
    super();

    this.backCommandService = options?.backCommandService;
    this.sendCommandService = new SendCommandService({
      message: dictionary.settings.projects.project.edit.invalid,
    });
    this.projectName = options?.projectName;
  }

  setOptions(opts) {
    super.setOptions(opts);

    const options = this.getOptions();

    this.backCommandService?.setOptions(options);
    this.sendCommandService?.setOptions(options);

    return this;
  }

  public async activate(msg: TelegramBot.Message): Promise<boolean> {
    const {
      text: projectDescription,
      chat: { id: chatId },
    } = msg;
    if (projectDescription == null) {
      return false;
    }

    if (projectDescription.length > MAX_SYMBOLS_IN_PROJECT_DESCRIPTION) {
      this.sendCommandService.setOptions({
        message: dictionary.settings.projects.project.edit.invalid,
      });
      await this.sendCommandService.action(msg);

      return false;
    }

    const { projectName } = this;

    const user = await getUser(chatId);
    await updateUser(user.chatId, {
      settings: {
        projects: user.settings.projects.map((project) => {
          if (project.name === projectName) {
            return {
              ...project,
              description: projectDescription,
            };
          }

          return project;
        }),
      },
    });

    await this.backCommandService?.action(msg);

    return true;
  }
}
