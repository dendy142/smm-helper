import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { SendCommandService } from '../commands/send.command';
import { BackCommandService } from '../commands/back.command';
import { dictionary } from '../../utils/dictionary';
import { getUser, updateUser } from '../../models/user.model';
import { MAX_SYMBOLS_IN_PROJECT_NAME, MAX_WORDS_IN_PROJECT_NAME } from '../../utils/constants';

export class ProjectInputHandlerService extends InputHandlerServiceTool {
  private readonly sendCommandService: SendCommandService;
  private readonly backCommandService?: BackCommandService;

  constructor(options?: { backCommandService?: BackCommandService }) {
    super();

    this.backCommandService = options?.backCommandService;
    this.sendCommandService = new SendCommandService({
      message: dictionary.settings.projects.invalid,
    });
  }

  setOptions(opts) {
    super.setOptions(opts);

    const options = this.getOptions();

    this.backCommandService?.setOptions(options);
    this.sendCommandService?.setOptions(options);

    return this;
  }

  public async activate(msg: TelegramBot.Message): Promise<boolean> {
    const {
      text: projectName,
      chat: { id: chatId },
    } = msg;
    if (projectName == null) {
      return false;
    }

    if (projectName.length > MAX_SYMBOLS_IN_PROJECT_NAME || projectName.split(' ').length > MAX_WORDS_IN_PROJECT_NAME) {
      this.sendCommandService.setOptions({
        message: dictionary.settings.projects.invalid,
      });
      await this.sendCommandService.action(msg);

      return false;
    }

    const user = await getUser(chatId);
    await updateUser(user.chatId, {
      settings: {
        projects: [
          ...user.settings.projects,
          {
            name: projectName,
          },
        ],
      },
    });

    await this.backCommandService?.action(msg);

    return true;
  }
}
