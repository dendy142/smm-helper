import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { SendCommandService } from '../commands/send.command';
import { BackCommandService } from '../commands/back.command';
import { dictionary } from '../../utils/dictionary';
import { getUser, updateUser } from '../../models/user.model';
import { DELETE_KEY_WORD } from '../../utils/constants';

export class ProjectDeleteInputHandlerService extends InputHandlerServiceTool {
  private readonly sendCommandService: SendCommandService;
  private readonly backCommandService?: BackCommandService;
  private readonly projectName: string;

  constructor(options: { backCommandService?: BackCommandService; projectName: string }) {
    super();

    this.backCommandService = options?.backCommandService;
    this.sendCommandService = new SendCommandService({
      message: dictionary.settings.projects.project.delete.invalid,
    });
    this.projectName = options?.projectName;
  }

  setOptions(opts) {
    super.setOptions(opts);

    const options = this.getOptions();

    this.backCommandService?.setOptions(options);
    this.sendCommandService?.setOptions(options);

    return this;
  }

  public async activate(msg: TelegramBot.Message): Promise<boolean> {
    const {
      text: keyWord,
      chat: { id: chatId },
    } = msg;
    if (keyWord?.toLowerCase() !== DELETE_KEY_WORD.toLowerCase()) {
      this.sendCommandService.setOptions({
        message: dictionary.settings.projects.project.delete.invalid,
      });
      await this.sendCommandService.action(msg);

      return false;
    }

    const { projectName } = this;

    const user = await getUser(chatId);
    const { settings } = user;
    const { projects } = settings;
    const projectIndex = projects.findIndex((project) => project.name === projectName);

    if (projectIndex === -1) {
      throw new Error('Project not found');
    }

    if (settings.currentProjectName === projects[projectIndex].name) {
      settings.currentProjectName = null;
    }

    projects.splice(projectIndex, 1);
    await updateUser(chatId, {
      settings,
    });

    await this.backCommandService?.action(msg);

    return true;
  }
}
