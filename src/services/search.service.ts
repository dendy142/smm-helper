import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import TelegramBot, { InlineKeyboardButton } from 'node-telegram-bot-api';
import { CommandsStructureFunction } from '../types/CommadsStructure.type';
import { find } from 'string-search';
import { updateUser } from '../models/user.model';
import { dictionary } from '../utils/dictionary';
import { toCommandsStructure } from '../utils/data';

const SEARCH_SUGGESTS_COUNT = 4;

export class SearchService extends ClusterServiceTool {
  constructor(private readonly structure: CommandsStructureFunction) {
    super();
  }

  async onMessage({ text, chat: { id: chatId } }: TelegramBot.Message): Promise<boolean> {
    const { telegramBot, context } = this.getOptions();

    const { structure } = this.validateProperties({
      structure: this.structure,
    });

    if (text == null) {
      return false;
    }

    const matches = await this.findMatches(text, structure);

    let messageResponse;
    if (matches.length > 0) {
      messageResponse = await telegramBot.sendMessage(chatId, dictionary.placeholders.searchVariants(text), {
        reply_markup: {
          inline_keyboard: matches.slice(0, SEARCH_SUGGESTS_COUNT).reduce<InlineKeyboardButton[][]>(
            (rows, match, index) => {
              let arrToPush;
              if (index % 2 === 0) {
                arrToPush = rows[0];
              } else {
                arrToPush = rows[1];
              }

              arrToPush.push({
                text: match,
                callback_data: match,
              });

              return rows;
            },
            [[], []],
          ),
        },
      });
    } else {
      messageResponse = await telegramBot.sendMessage(chatId, dictionary.placeholders.nothingFound(text), {
        reply_markup: {
          inline_keyboard: [
            [
              {
                text: dictionary.help.commandName,
                callback_data: dictionary.help.commandName,
              },
            ],
          ],
        },
      });
    }

    setTimeout(async () => {
      await updateUser(chatId, {
        $push: { removeMessagesIds: messageResponse.message_id },
      });
    }, 0);

    return true;
  }

  private async findMatches(text: string, structure: CommandsStructureFunction): Promise<string[]> {
    const { context } = this.getOptions();

    const result = new Set<string>();

    const commandsStructure = await toCommandsStructure(structure, context);
    for (const commandStructure of commandsStructure) {
      if (commandStructure.search != null) {
        const search = commandStructure.search instanceof Array ? commandStructure.search : [commandStructure.search];

        for (const searchItem of search) {
          if ((await find(searchItem, text)).length > 0) {
            if (typeof commandStructure.commandName === 'function') {
              result.add(await commandStructure.commandName());
            } else {
              result.add(commandStructure.commandName);
            }
          }
        }
      }

      const deepStructure = commandStructure.structure;
      if (deepStructure != null) {
        for (const match of await this.findMatches(text, deepStructure)) {
          result.add(match);
        }
      }
    }

    return Array.from(result);
  }
}
