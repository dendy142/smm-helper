import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { InputHandlerServiceTool } from '../types/tools/InputHandlerService.tool';

export class InputHandlerService extends ClusterServiceTool {
  private service: InputHandlerServiceTool | null = null;

  async onMessage(msg): Promise<boolean> {
    const service = this.service;
    if (service == null) {
      return false;
    }

    service.setOptions(this.getOptions());
    const result = await service.activate(msg);
    if (result) {
      this.service = null;
    }

    return true;
  }

  async onStopPropagation() {
    this.service = null;
  }

  public activateService(service: InputHandlerServiceTool): void {
    service.setOptions(this.getOptions());

    this.service = service;
  }
}
