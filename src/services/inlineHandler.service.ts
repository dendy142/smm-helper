import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import TelegramBot from 'node-telegram-bot-api';
import {
  CHANGE_LANGUAGE_COMMAND,
  FORMATS,
  REPEAT_COMPLETION_COMMAND,
  TONES,
  USE_PLAGIARISM_CHECKED_COMMAND,
  USE_TAGS_COMMAND,
} from '../utils/constants';
import { getUser, updateUser } from '../models/user.model';
import { sendCompletionWithSideEffects } from '../utils/ai';
import { getNextLanguage } from '../utils/settings';
import { getUserPostSettingsInlineKeyboard } from './commands/postSettings.command';
import { EventNames } from '../types/ClusterContext.type';
import { getFormatsInlineKeyboard } from './commands/formatSettings.command';
import { getTonesInlineKeyboard } from './commands/toneSettings.command';
import { dictionary } from '../utils/dictionary';

const tones = new Map(Object.entries(TONES).map((arr) => arr.reverse()) as [string, string][]);
const formats = new Map(Object.entries(FORMATS).map((arr) => arr.reverse()) as [string, string][]);

export class InlineHandlerService extends ClusterServiceTool {
  async onCallbackQuery(query: TelegramBot.CallbackQuery) {
    const { data, message } = query;
    if (data == null || message?.chat == null) {
      return false;
    }

    const { context } = this.getOptions();
    const { telegramBot } = this.validateProperties({
      telegramBot: this.telegramBot,
    });

    if (data === REPEAT_COMPLETION_COMMAND) {
      if (message.text == null || message.reply_to_message == null) {
        return false;
      }

      const {
        chat: { id: chatId },
        reply_to_message: { message_id },
        text,
      } = message;

      const user = await getUser(chatId);

      try {
        if (user.structureMessageId != null) {
          await telegramBot.deleteMessage(chatId, user.structureMessageId.toString());
        }
      } catch (err) {}
      await updateUser(chatId, {
        structureMessageId: null,
      });

      context.dontRemoveMessage = true;

      await sendCompletionWithSideEffects(telegramBot, user, message_id, text, () => {
        context.dispatchCallbackQuery(context, {
          ...query,
          data: dictionary.backBtn,
        });
      }).catch(() => {
        context.dispatchCallbackQuery(context, {
          ...query,
          data: dictionary.backBtn,
        });
      });

      return true;
    }

    if ([CHANGE_LANGUAGE_COMMAND, USE_TAGS_COMMAND, USE_PLAGIARISM_CHECKED_COMMAND].includes(data)) {
      const {
        chat: { id: chatId },
        text,
        message_id,
      } = message;

      if (text == null) {
        return false;
      }

      const user = await getUser(chatId);

      if (data === CHANGE_LANGUAGE_COMMAND) {
        user.settings.language = getNextLanguage(user);
      }
      if (data === USE_TAGS_COMMAND) {
        user.settings.useTags = !user.settings.useTags;
      }
      if (data === USE_PLAGIARISM_CHECKED_COMMAND) {
        user.settings.usePlagiarismChecker = !user.settings.usePlagiarismChecker;
      }

      await updateUser(chatId, {
        settings: user.settings,
        $pull: {
          removeMessagesIds: message_id,
        },
      });
      context.dontRemoveMessage = true;

      const removeEventListener = context.addEventListener(EventNames.loopEnd, async () => {
        await updateUser(chatId, {
          $push: {
            removeMessagesIds: message_id,
          },
        });
        removeEventListener();
      });

      await telegramBot.editMessageText(text, {
        message_id,
        chat_id: chatId,
        reply_markup: {
          inline_keyboard: getUserPostSettingsInlineKeyboard(user),
        },
      });

      return true;
    }

    if (tones.has(data) || formats.has(data)) {
      if (message.text == null) {
        return false;
      }

      const {
        chat: { id: chatId },
        message_id,
        text,
      } = message;

      const user = await getUser(chatId);

      if (tones.has(data)) {
        const toneName = tones.get(data);
        if (toneName == null) {
          return false;
        }

        const isToneSet = user.settings.tone === toneName;
        user.settings.tone = isToneSet ? null : toneName;
        await updateUser(chatId, {
          settings: user.settings,
          $pull: {
            removeMessagesIds: message_id,
          },
        });

        await telegramBot.editMessageText(text, {
          message_id,
          chat_id: chatId,
          reply_markup: {
            inline_keyboard: getTonesInlineKeyboard(user),
          },
        });
      }
      if (formats.has(data)) {
        const formatName = formats.get(data);
        if (formatName == null) {
          return false;
        }
        const isFormatSet = user.settings.format === formatName;

        user.settings.format = isFormatSet ? null : formatName;
        await updateUser(chatId, {
          settings: user.settings,
          $pull: {
            removeMessagesIds: message_id,
          },
        });

        await telegramBot.editMessageText(text, {
          message_id,
          chat_id: chatId,
          reply_markup: {
            inline_keyboard: getFormatsInlineKeyboard(user),
          },
        });
      }

      context.dontRemoveMessage = true;

      const removeEventListener = context.addEventListener(EventNames.loopEnd, async () => {
        await updateUser(chatId, {
          $push: {
            removeMessagesIds: message_id,
          },
        });
        removeEventListener();
      });
    }

    return false;
  }
}
