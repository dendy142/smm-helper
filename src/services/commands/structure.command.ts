import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import TelegramBot, { InlineKeyboardButton } from 'node-telegram-bot-api';
import { dictionary } from '../../utils/dictionary';
import { CommandsStructureFunction, CommandsStructure } from '../../types/CommadsStructure.type';
import { getUser, updateUser } from '../../models/user.model';
import { FilterQuery } from 'mongoose';
import { User } from '../../types/Schemas.type';
import { EventNames } from '../../types/ClusterContext.type';
import { asyncReduce, toCommandsStructure, toMessageObject } from '../../utils/data';
import { UnionMessageType } from '../../types/Message.type';

export class StructureCommandService extends CommandServiceTool {
  private readonly structure?: CommandsStructureFunction;
  private message: UnionMessageType | null = null;

  constructor(options?: { message?: UnionMessageType; structure?: CommandsStructureFunction }) {
    super();

    this.message = options?.message ?? null;
    this.structure = options?.structure;
  }

  setOptions(options: Parameters<CommandServiceTool['setOptions']>[number] & { message?: UnionMessageType | null }) {
    super.setOptions(options);

    this.message = options.message === null ? null : options.message ?? this.message;

    return this;
  }

  async action(msg: TelegramBot.Message, commandStructure: CommandsStructure[0]) {
    const { telegramBot, context } = this.getOptions();

    const {
      chat: { id: chatId },
    } = msg;

    const message = await toMessageObject(
      this.message ?? dictionary.placeholders.choose(commandStructure.commandName as string),
      context,
    );

    const { keyboardType, structureMessageId } = await getUser(chatId);

    const structure =
      this.structure != null || this.commandsStructure !== null
        ? await toCommandsStructure(this.structure ?? commandStructure.structure!, context)
        : null;
    const messageOptions: TelegramBot.SendMessageOptions = {
      ...message,
      // @ts-ignore
      reply_markup: {
        ...message.reply_markup,
        [keyboardType === 'default' ? 'keyboard' : 'inline_keyboard']: (
          await asyncReduce<CommandsStructure[number], InlineKeyboardButton[][]>(
            structure ?? [],
            async (acc, { commandName, hidden, row }) => {
              if (typeof hidden === 'function' ? await hidden(msg) : hidden) {
                return acc;
              }

              if (acc[row] == null) {
                acc[row] = [];
              }

              let command;
              if (typeof commandName === 'function') {
                command = commandName();
              } else {
                command = commandName;
              }

              if (keyboardType === 'default') {
                acc[row].push({ text: command });
              } else {
                acc[row].push({ text: command, callback_data: command });
              }

              return acc;
            },
            [],
          )
        ).filter(Boolean),
      },
    };

    const sendMessage = async (text: string) => {
      return await telegramBot.sendMessage(chatId, text, messageOptions);
    };

    const messageText = message.text;
    let messageResponse;
    if (structureMessageId == null) {
      messageResponse = await sendMessage(messageText);
    } else {
      try {
        // @ts-ignore
        messageResponse = await telegramBot.editMessageText(messageText, {
          ...messageOptions,
          chat_id: chatId,
          message_id: structureMessageId,
        });
      } catch (err) {
        messageResponse = await sendMessage(messageText);
      }
    }

    if (commandStructure.pin === true) {
      await telegramBot.pinChatMessage(chatId, messageResponse.message_id);
    }

    await updateUser(chatId, {
      structureMessageId: messageResponse.message_id,
    });

    if (commandStructure.saveMessage !== true) {
      const removeListener = context.addEventListener(EventNames.loopEnd, () => {
        removeListener();

        void updateUser(chatId, {
          $push: {
            removeMessagesIds: messageResponse.message_id,
          },
        });
      });
    } else {
      const { savedMessages } = await getUser(chatId);

      let command;
      if (typeof commandStructure.commandName === 'function') {
        command = commandStructure.commandName();
      } else {
        command = commandStructure.commandName;
      }

      const request: FilterQuery<User> = {
        savedMessages: [
          ...savedMessages,
          {
            commandName: command,
            messageId: messageResponse.message_id,
          },
        ],
      };
      for (const message of savedMessages) {
        const { commandName, messageId } = message;
        if (commandName === commandStructure.commandName) {
          message.messageId = messageResponse.message_id;
          request.savedMessages = savedMessages;

          request.$push = {
            removeMessagesIds: messageId,
          };
          break;
        }
      }

      await updateUser(chatId, request);
    }
  }
}
