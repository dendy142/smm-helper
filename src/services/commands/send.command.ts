import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { getUser, updateUser } from '../../models/user.model';
import { FilterQuery } from 'mongoose';
import { User } from '../../types/Schemas.type';
import { EventNames } from '../../types/ClusterContext.type';
import { dictionary } from '../../utils/dictionary';
import { toMessageObject } from '../../utils/data';
import { UnionMessageType } from '../../types/Message.type';

export class SendCommandService extends CommandServiceTool {
  isMessageSender = true;
  private readonly saveMessage?: boolean;
  private message: UnionMessageType | null = null;

  constructor(options?: { message?: UnionMessageType | null; saveMessage?: boolean }) {
    super();

    this.message = options?.message || dictionary.placeholders.sendCommandService;
    this.saveMessage = options?.saveMessage;
  }

  setOptions(options: Parameters<CommandServiceTool['setOptions']>[number] & { message?: UnionMessageType | null }) {
    super.setOptions(options);

    this.message = options?.message === null ? null : options?.message ?? this.message;

    return this;
  }

  async action({ chat: { id: chatId } }) {
    const { telegramBot, context } = this.getOptions();

    let { message } = this.validateProperties({
      message: this.message,
    });
    message = await toMessageObject(message, context);

    const { messageText } = this.validateProperties({
      messageText: message?.text,
    });

    const messageResponse = await telegramBot.sendMessage(chatId, messageText, {
      parse_mode: 'HTML',
      ...message,
    });

    if (this.saveMessage !== true) {
      const removeListener = context.addEventListener(EventNames.loopEnd, () => {
        removeListener();

        void updateUser(chatId, {
          $push: {
            removeMessagesIds: messageResponse.message_id,
          },
        });
      });
    } else {
      const { savedMessages } = await getUser(chatId);

      const request: FilterQuery<User> = {
        savedMessages: [
          ...savedMessages,
          {
            commandName: messageText,
            messageId: messageResponse.message_id,
          },
        ],
      };
      for (const savedMessage of savedMessages) {
        const { commandName, messageId } = savedMessage;
        if (commandName === messageText) {
          savedMessage.messageId = messageResponse.message_id;
          request.savedMessages = savedMessages;

          request.$push = {
            removeMessagesIds: messageId,
          };
          break;
        }
      }

      await updateUser(chatId, request);
    }
  }
}
