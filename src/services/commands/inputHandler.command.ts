import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { InputHandlerService } from '../inputHandler.service';
import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import { EventNames } from '../../types/ClusterContext.type';

export class InputHandlerCommandService extends CommandServiceTool {
  constructor(
    private readonly inputHandler: InputHandlerService,
    private readonly inputHandlerService: InputHandlerServiceTool,
  ) {
    super();
  }

  public async action() {
    const { context } = this.getOptions();

    const { inputHandlerService, inputHandler } = this;

    const removeListener = context.addEventListener(EventNames.loopEnd, () => {
      removeListener();

      const options = this.getOptions();
      inputHandlerService.setOptions(options);
      inputHandler.setOptions(options);

      inputHandler.activateService(inputHandlerService);
    });
  }
}
