import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { dictionary } from '../../utils/dictionary';
import { getUser } from '../../models/user.model';
import { FORMATS, TONES } from '../../utils/constants';
import { InlineKeyboardButton } from 'node-telegram-bot-api';
import { SendCommandService } from './send.command';

export const getTonesInlineKeyboard = (user) => {
  return Object.entries(TONES).reduce<InlineKeyboardButton[][]>(
    (acc, [tone, command], index) => {
      const tonesLength = Object.keys(TONES).length;
      const arrIndex = Math.floor((index / tonesLength) * 3);

      acc[arrIndex].push({
        text: dictionary.generate.tones[tone](user.settings.tone === tone),
        callback_data: command,
      });

      return acc;
    },
    [[], [], []],
  );
};

export class ToneSettingsCommand extends CommandServiceTool {
  private readonly sendCommandService = new SendCommandService();

  setOptions(options): this {
    super.setOptions(options);

    this.sendCommandService.setOptions(options);

    return this;
  }

  public async action(msg) {
    const { context } = this.getOptions();
    const { sendCommandService } = this;

    const user = await getUser(context.chatId);

    sendCommandService.setOptions({
      message: {
        text: dictionary.settings.post.message,
        reply_markup: {
          inline_keyboard: getTonesInlineKeyboard(user),
        },
      },
    });
    await sendCommandService.action(msg);
  }
}
