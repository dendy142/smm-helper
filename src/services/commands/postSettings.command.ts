import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { getUser } from '../../models/user.model';
import { SendCommandService } from './send.command';
import { dictionary } from '../../utils/dictionary';
import { CHANGE_LANGUAGE_COMMAND, USE_PLAGIARISM_CHECKED_COMMAND, USE_TAGS_COMMAND } from '../../utils/constants';
import { User } from '../../types/Schemas.type';
import { Languages } from '../../utils/settings';

export const getUserPostSettingsInlineKeyboard = (user: User) => [
  [
    {
      text: dictionary.settings.post.language(Languages[user.settings.language]),
      callback_data: CHANGE_LANGUAGE_COMMAND,
    },
  ],
  [
    {
      text: dictionary.settings.post.useTags(user.settings.useTags),
      callback_data: USE_TAGS_COMMAND,
    },
  ],
  [
    {
      text: dictionary.settings.post.usePlagiarismChecker(user.settings.usePlagiarismChecker),
      callback_data: USE_PLAGIARISM_CHECKED_COMMAND,
    },
  ],
];

export class PostSettingsCommandService extends CommandServiceTool {
  private readonly sendCommandService = new SendCommandService();

  setOptions(options): this {
    super.setOptions(options);

    this.sendCommandService.setOptions(options);

    return this;
  }

  async action(msg): Promise<void> {
    const { context } = this.getOptions();
    const { sendCommandService } = this;

    const user = await getUser(context.chatId);

    sendCommandService.setOptions({
      message: {
        text: dictionary.settings.post.message,
        reply_markup: {
          inline_keyboard: getUserPostSettingsInlineKeyboard(user),
        },
      },
    });
    await sendCommandService.action(msg);
  }
}
