import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { dictionary } from '../../utils/dictionary';
import { getUser } from '../../models/user.model';
import { FORMATS } from '../../utils/constants';
import { InlineKeyboardButton } from 'node-telegram-bot-api';
import { SendCommandService } from './send.command';

export const getFormatsInlineKeyboard = (user) => {
  return Object.entries(FORMATS).reduce<InlineKeyboardButton[][]>(
    (acc, [format, command], index) => {
      const formatsLength = Object.keys(FORMATS).length;
      const arrIndex = Math.floor((index / formatsLength) * 3);

      acc[arrIndex].push({
        text: dictionary.generate.formats[format](user.settings.format === format),
        callback_data: command,
      });

      return acc;
    },
    [[], [], []],
  );
};

export class FormatSettingsCommand extends CommandServiceTool {
  private readonly sendCommandService = new SendCommandService();

  setOptions(options): this {
    super.setOptions(options);

    this.sendCommandService.setOptions(options);

    return this;
  }

  public async action(msg) {
    const { context } = this.getOptions();
    const { sendCommandService } = this;

    const user = await getUser(context.chatId);

    sendCommandService.setOptions({
      message: {
        text: dictionary.settings.post.message,
        reply_markup: {
          inline_keyboard: getFormatsInlineKeyboard(user),
        },
      },
    });
    await sendCommandService.action(msg);
  }
}
