import { CommandServiceTool } from '../../types/tools/CommandService.tool';

export class AggregatorCommandService extends CommandServiceTool {
  constructor(private readonly commandServices: CommandServiceTool[]) {
    super();
  }

  async action(msg, commandStructure): Promise<void> {
    for (const service of this.commandServices) {
      service.setOptions(this.getOptions());
      await service.action(msg, commandStructure);
    }
  }
}
