import { CommandsStructureFunction } from '../../types/CommadsStructure.type';
import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { updateUser } from '../../models/user.model';
import { User } from '../../types/Schemas.type';
import { BackCommandService } from './back.command';
import { ServiceTool } from '../../types/tools/Service.tool';

export class UserUpdateCommand extends CommandServiceTool {
  private readonly propertyName: keyof User;
  private readonly prepareCallback: (text?: string) => Promise<unknown> | unknown;
  private readonly backCommandService: BackCommandService;

  constructor(options: {
    propertyName: keyof User;
    prepareCallback: (text?: string) => Promise<unknown> | unknown;
    backCommandService: BackCommandService;
  }) {
    super();

    this.propertyName = options.propertyName;
    this.prepareCallback = options.prepareCallback;
    this.backCommandService = options.backCommandService;
  }

  setOptions(
    options: Parameters<ServiceTool['setOptions']>[number] & { commandsStructure?: CommandsStructureFunction },
  ): this {
    const result = super.setOptions(options);

    this.backCommandService.setOptions(options);

    return result;
  }

  async action(msg: TelegramBot.Message) {
    const chatId = msg.chat.id;

    await updateUser(chatId, {
      [this.propertyName]: await this.prepareCallback(msg.text),
    });

    await this.backCommandService.action(msg);
  }
}
