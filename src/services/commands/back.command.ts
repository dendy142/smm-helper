import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { StructureCommandService } from './structure.command';
import { findCommandStructure } from '../../utils/data';
import { UnionMessageType } from '../../types/Message.type';

export class BackCommandService extends CommandServiceTool {
  private readonly to: string;
  private readonly structureCommandService = new StructureCommandService();
  private message: UnionMessageType | null = null;

  constructor(options: { to: string; message?: UnionMessageType }) {
    super();

    this.to = options.to;
    this.message = options.message === null ? null : options.message ?? this.message;
  }

  setOptions(options: Parameters<CommandServiceTool['setOptions']>[number] & { message?: UnionMessageType }) {
    super.setOptions(options);

    this.message = options.message === null ? null : options.message ?? this.message;

    return this;
  }

  async action(msg) {
    const { commandsStructure, context } = this.getOptions();

    const { commandStructure } = this.validateProperties({
      commandStructure: await findCommandStructure(msg, this.to, commandsStructure!, context),
    });

    return this.getStructureCommandService().action(msg, {
      ...commandStructure,
      saveMessage: false,
    });
  }

  private getStructureCommandService(): StructureCommandService {
    this.structureCommandService.setOptions({
      ...this.getOptions(),
      message: this.message,
    });

    return this.structureCommandService;
  }
}
