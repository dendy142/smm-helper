import { getUser } from '../models/user.model';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { SendCommandService } from './commands/send.command';
import TelegramBot from 'node-telegram-bot-api';

export class PreventCommandService extends ClusterServiceTool {
  private readonly sendCommandService = new SendCommandService();

  constructor(private readonly message: string) {
    super();
  }

  setOptions(options): this {
    super.setOptions(options);

    this.sendCommandService.setOptions(options);

    return this;
  }

  public async onMessage(message) {
    const options = this.getOptions();

    const user = await getUser(options.context.chatId);
    if (user.isWaitingForResponse) {
      const { sendCommandService } = this;
      sendCommandService.setOptions({
        ...options,
        message: this.message,
      });
      await sendCommandService.action(message);

      options.context.dontRemoveMessage = true;
    }

    return user.isWaitingForResponse;
  }

  async onCallbackQuery(query: TelegramBot.CallbackQuery) {
    if (query.message != null) {
      return this.onMessage(query.message);
    }

    return false;
  }
}
