import TelegramBot from 'node-telegram-bot-api';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { CommandsStructureFunction, CommandsStructure } from '../types/CommadsStructure.type';
import { StructureCommandService } from './commands/structure.command';
import { CommandServiceTool } from '../types/tools/CommandService.tool';
import { findCommandStructure } from '../utils/data';
import { ClusterContext } from '../types/ClusterContext.type';

export class CommandsService extends ClusterServiceTool {
  private readonly commandsStructure: CommandsStructureFunction;
  private readonly defaultCommands?: {
    command: string;
    description: string;
  }[];
  private isDefaultCommandsSet: boolean = false;
  private structureCommandService = new StructureCommandService();

  constructor(options: {
    structure: CommandsStructureFunction;
    defaultCommands?: {
      command: string;
      description: string;
    }[];
  }) {
    super();

    this.defaultCommands = options.defaultCommands;
    this.commandsStructure = options.structure;
  }

  setOptions(options: { telegramBot: TelegramBot; context: ClusterContext }) {
    super.setOptions(options);

    void this.setDefaultCommands();

    return this;
  }

  async onMessage(msg): Promise<boolean> {
    if (!this.validateMessage(msg)) {
      return false;
    }

    const { context } = this.getOptions();
    const { text: command } = msg;

    const commandStructure = await findCommandStructure(msg, command, this.commandsStructure, context);
    context.commandStructure = commandStructure ?? context.commandStructure;

    const isAvailable = (await commandStructure?.commandCondition?.isTrue(msg)) ?? true;
    if (!isAvailable) {
      await this.actionOnCommandService(commandStructure!.commandCondition!, msg, commandStructure!);
    } else {
      if (commandStructure?.commandService != null) {
        const services =
          commandStructure.commandService instanceof Array
            ? commandStructure.commandService
            : [commandStructure.commandService];

        let removeStructure: boolean = false;
        for (const service of services) {
          await this.actionOnCommandService(service, msg, commandStructure);
          removeStructure = removeStructure || service.isMessageSender;
        }
      }

      if (commandStructure?.structure != null) {
        if (commandStructure.structureMessage != null) {
          this.structureCommandService.setOptions({
            message: commandStructure.structureMessage,
          });
        }

        await this.actionOnCommandService(this.structureCommandService, msg, commandStructure);

        if (commandStructure.structureMessage != null) {
          this.structureCommandService.setOptions({
            message: null,
          });
        }
      }
    }

    return commandStructure != null;
  }

  async onCallbackQuery(query) {
    return this.onMessage({
      ...query.message,
      text: query.data,
    });
  }

  private actionOnCommandService(
    commandService: CommandServiceTool,
    msg: TelegramBot.Message,
    structure: CommandsStructure[number],
  ) {
    commandService.setOptions({
      ...this.getOptions(),
      commandsStructure: this.commandsStructure,
    });
    return commandService.action(msg, structure);
  }

  private validateMessage(msg: TelegramBot.Message): msg is TelegramBot.Message & { text: string } {
    return msg.text != null;
  }

  private async setDefaultCommands(): Promise<boolean> {
    const { defaultCommands, isDefaultCommandsSet } = this;
    if (defaultCommands == null || isDefaultCommandsSet) {
      return false;
    }

    this.isDefaultCommandsSet = true;

    return true;
  }
}
