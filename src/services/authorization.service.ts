import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { createUser, getUser, updateUsers } from '../models/user.model';

export class AuthorizationService extends ClusterServiceTool {
  private cachedUsers = new Set<number>();

  constructor() {
    super();

    void updateUsers(
      {},
      {
        isWaitingForResponse: false,
      },
    );
  }

  async onMessage({ message_id, chat: { id: chatId } }: TelegramBot.Message) {
    const cachedUsers = this.cachedUsers;
    if (cachedUsers.has(chatId)) {
      return false;
    } else {
      cachedUsers.add(chatId);
    }

    try {
      await getUser(chatId);
    } catch (err) {
      await createUser(chatId);
    }

    return false;
  }

  async onCallbackQuery(query: TelegramBot.CallbackQuery) {
    if (query.message != null) {
      return this.onMessage(query.message);
    }

    return false;
  }
}
