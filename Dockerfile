FROM node:17

ENV NODE_ENV development

WORKDIR /app

EXPOSE 9229

CMD bash -c "npm i && npm run start"
